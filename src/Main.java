import java.util.Scanner;

import Model.Rectangle;
import Model.StatusMessage;
import Utility.Drawer;
import Utility.InputValidator;
import Utility.RectangleChecker;

public class Main {

    public static void mainLoop() {
        
        boolean running = true;

        Scanner input = new Scanner(System.in);

        while (running){

            System.out.print("Enter height of rectangle: ");
            String heightMsg = input.nextLine();
            System.out.print("Enter length of rectangle: ");
            String lengthMsg = input.nextLine();

            //Input validation of height and length input from user
            StatusMessage heightStatus = InputValidator.messageCreation(heightMsg);
            StatusMessage lengthStatus = InputValidator.messageCreation(lengthMsg);
            if (heightStatus.getStatus() == false){
                System.out.println(heightStatus.getMessage());
                running = false;
            }
            if (lengthStatus.getStatus() == false){
                System.out.println(lengthStatus.getMessage());
                running = false;
            }
            
            //Create a rectangle object based on user input
            Rectangle rectangle = new Rectangle(Integer.valueOf(heightMsg), Integer.valueOf(lengthMsg));
            
            //You'll never guess what this static method does
            if (RectangleChecker.rectangleHeightAndLengthAreDifferent(rectangle) == false){
                System.out.println("Not shaped like a rectangle");
                running = false;
            }
            
            System.out.print("Enter sub rectangle height: ");
            String subHeightMsg = input.nextLine();
            System.out.print("Enter sub rectangle length: ");
            String subLengthMsg = input.nextLine();
            
            //Input validation and custom messages
            StatusMessage subLengthStatus = InputValidator.messageCreation(subLengthMsg);
            StatusMessage subHeightStatus = InputValidator.messageCreation(subHeightMsg);
            if (subHeightStatus.getStatus() == false){
                System.out.println(subHeightStatus.getStatus());
            }
            if (subLengthStatus.getStatus() == false){
                System.out.println(subLengthStatus.getMessage());
            }

            //Calculate padding between inner and outer rectangle
            int paddingH = (rectangle.getHeight() - Integer.valueOf(subHeightMsg))/2;
            int paddingL = (rectangle.getLength() - Integer.valueOf(subLengthMsg))/2;

            //Check if the inner rectangle matches the criteria set (symmetric, smaller etc etc) and draw the rectangle
            boolean isInnerPossible = RectangleChecker.canCreateSubRectangle(rectangle, Integer.valueOf(subHeightMsg), Integer.valueOf(subLengthMsg), paddingH, paddingL);
            Drawer.drawRectangle(rectangle.getHeight(), rectangle.getLength(), isInnerPossible, paddingH, paddingL);

            //Doing this instead of creating custom messages
            if (!isInnerPossible){
                System.out.println("\nCouldn't create sub rect on those values");
            }

            //Exit message with error handling and custom messages
            System.out.print("\nWanna go again? Y/N: ");
            String exitChoice = input.nextLine();
            if (InputValidator.validateExitChoice(exitChoice) == false || exitChoice.toUpperCase().equals("N")){
                System.out.println("Thanks for playing");
                running = false;
            }
        }

        input.close();
    }

    public static void main(String[] args) throws Exception {
        //Just used for calling the main loop
        mainLoop();        
    }
}
