package Model;

public class Rectangle {

    /**
     * Model class for rectangle
     */
    int height;
    int length;

    public Rectangle(int height, int length){
        this.height = height;
        this.length = length;
    }

    public int getHeight() {
        return height;
    }

    public int getLength() {
        return length;
    }
}
