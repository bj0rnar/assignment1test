package Model;

public class StatusMessage {
    
    /**
     * Custom class for creating an object with both boolean and String
     * Pseudo Tuple
     */
    
    String message;
    boolean status;
    
    public StatusMessage(String message, boolean status){
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public boolean getStatus() {
        return status;
    }
}
