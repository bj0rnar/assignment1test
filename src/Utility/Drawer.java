package Utility;

public class Drawer {
    //Draw rectangle based (and sub rectangle) based on given values
    public static void drawRectangle(int height, int length, boolean isInnerPossible, int paddingH, int paddingL) {
        for (int i = 0; i < height; i++){
            //Go down one
            System.out.println("");
            for (int j = 0; j < length; j++){
                
                //Covers the top and bottom of the outer rectangle
                if (i == 0 || i == height-1){
                    System.out.print("#");
                }
                //Covers the sides of the outer rectangle
                else if (j == 0 || j == length-1) {
                    System.out.print("#");
                }
                //While inner rectangle is on Y (measures from padding or subtracting padding) and X is between lower and higher bounds
                //This covers the inner rectangle top and bottom
                else if ((i == paddingH || i == (height-1) - paddingH) && (j >= paddingL && j <= (length-1) - paddingL) && isInnerPossible){
                    System.out.print("#");
                }
                //The exact opposite of the above
                //Covers the inner rectangle sides
                else if ((j == paddingL || j == (length-1) - paddingL) && (i >= paddingH && i <= (height-1) - paddingH) && isInnerPossible){
                    System.out.print("#");
                }
                //In any other case just print a space
                else {
                    System.out.print(" ");
                }
            }
        }
    }
}
