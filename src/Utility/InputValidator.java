package Utility;

import Model.StatusMessage;

public class InputValidator {
    //Creates a StatusMessage object with boolean and message. Used for user feedback when
    //creating rectangle from user input
    public static StatusMessage messageCreation(String userInput){
        if (userInput.equals("")){
            return new StatusMessage("Empty input field", false);
        }

        if (canBeConverted(userInput) == false){
            return new StatusMessage("Only numeric values allowed", false);
        }

        if (Integer.valueOf(userInput) <= 0){
            return new StatusMessage("Positive values only", false);
        }

        return new StatusMessage("Success", true);
    }

    //Try catch method for converting String to int
    private static boolean canBeConverted(String input){
        try {
            Integer.valueOf(input);
            return true;
        }
        catch (NumberFormatException exception) {
            return false;
        }
    }

    //Exit choice has to be either of these
    public static boolean validateExitChoice(String choice){
        return choice.toUpperCase().equals("Y") || choice.toUpperCase().equals("N");
    }
}