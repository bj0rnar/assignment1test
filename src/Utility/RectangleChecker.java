package Utility;

import Model.Rectangle;

public class RectangleChecker {
    
    public static boolean canCreateSubRectangle(Rectangle rect, int userInputheight, int userInputlength, int paddingH, int paddingL) {
        //if rect height and sub height is not both even or both odd the sub rectangle won't be symmetric
        if ((rect.getHeight() % 2 == 0 ? true : false) != (userInputheight % 2 == 0 ? true : false)){
            return false;
        }
        
        //Same as above, except for length
        if ((rect.getLength() % 2 == 0 ? true : false) != (userInputlength % 2 == 0 ? true : false)){
            return false;
        }

        //Allow the smallest possible sub rectangle
        if ((userInputheight <= 2 && userInputlength <= 3) || (userInputheight <= 3 && userInputlength <= 2 )) {
            return false;
        }
        
        //if padding between rect and subrect is less than 2, no subrect can be created
        if (paddingH < 2 || paddingL < 2){
            return false;
        }

        //if all tests pass just return true
        return true;

    }

    public static boolean rectangleHeightAndLengthAreDifferent(Rectangle rect){
        return rect.getHeight() != rect.getLength();
    }

    public static boolean subRectIsSmallerThanOuterRect(Rectangle rect, int subHeight, int subLength){
        return rect.getHeight() > subHeight && rect.getLength() < subLength;
    }

}
